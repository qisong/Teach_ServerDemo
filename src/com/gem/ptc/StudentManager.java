package com.gem.ptc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import com.gem.ptc.teach.HbnUtil;
import com.gem.ptc.teach.student.Constants;
import com.gem.ptc.teach.student.Student;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class Entry
 */
@WebServlet("/Entry")
public class StudentManager extends HttpServlet {
	public static final int CURRENT_CLIENT_VERSION = 2;
	private static final long serialVersionUID = 1L;
	public static String IMAGE_FOLDER;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StudentManager() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) {

		response.setContentType("text/html");
		PrintWriter out;
		try {

			out = response.getWriter();
			response.setCharacterEncoding("UTF-8");

			ServletInputStream inputStream = request.getInputStream();
			int len = request.getContentLength();
			byte[] content = new byte[len];
			int readLen = inputStream.read(content);
			if (len != readLen) {
				out.println("sent length=" + len
						+ " but server received length=" + readLen);
			} else {
				String readString = new String(content, "UTF-8");
				Gson gson = new Gson();
				JsonParser parser = new JsonParser();
				JsonArray array = parser.parse(readString).getAsJsonArray();
				int actionId = gson.fromJson(array.get(0), int.class);
				switch (actionId) {
				case Constants.REQ_ADD_STUDENT:
					addStudent(out, readString, gson, array);
					break;
				case Constants.REQ_LIST_STUDENT:
					listStudent(out, gson);
				}

			}
			out.flush();
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void listStudent(PrintWriter out, Gson gson) {
		Session session = HbnUtil.getSession();
		List<Student> students = session.createQuery(
				"from Student student").list();

		session.close();
		out.write(gson.toJson(students));
	}

	private void addStudent(PrintWriter out, String readString, Gson gson,
			JsonArray array) {
		Student student = gson.fromJson(array.get(1), Student.class);

		// gson.from
		Session session = HbnUtil.getSession();
		session.beginTransaction();
		session.save(student);
		session.getTransaction().commit();
		out.println("received data:" + readString);
		session.close();
	}

}
