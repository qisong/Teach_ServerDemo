package com.gem.ptc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Entry
 */
@WebServlet("/login.do")
public class LoginManager extends HttpServlet {
	public static final int CURRENT_CLIENT_VERSION = 2;
	private static final long serialVersionUID = 1L;
	public static String IMAGE_FOLDER;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginManager() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) {
		String name = request.getParameter("name");
		String passwd = request.getParameter("passwd");
		HttpSession session = request.getSession();
		session.setAttribute("name", name);
		session.setAttribute("passwd", passwd);
		try {
			request.getRequestDispatcher("/jsp/main.jsp").forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
