package com.gem.ptc.teach;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HbnUtil {

	private static Configuration config = null;
	private static SessionFactory factory = null;
	private static Session session = null;

	static {
		try {
			config = new Configuration().configure();
			factory = config.buildSessionFactory();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Session getSession() {
		return factory.openSession();
	}

	public static void closeSession(Session session) {
		if (session != null) {
			session.close();
			session = null;
		}
	}
}
