package com.gem.ptc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCDemo {
	static String driver = "com.mysql.jdbc.Driver";
	static String url = "jdbc:mysql://localhost:3306/s_t";
	static String user = "root";
	static String password = "cdw76412";

	public static void main(String[] args) throws Exception {

		//selectDemo();
//		insertDemo();
		
//		deleteDemo();
		Connection conn = getConnection(driver, url, user, password);
		Statement st = conn.createStatement();
		st.execute("update student set sname='xyz' where sno='1'");
		st.close();
		conn.close();
	}

	private static void deleteDemo() throws ClassNotFoundException,
			SQLException {
		Connection conn = getConnection(driver, url, user, password);
		Statement st = conn.createStatement();
		st.executeUpdate("delete from student where sname='zhangbing'");
		st.close();
		conn.close();
	}

	private static void insertDemo() throws ClassNotFoundException,
			SQLException {
		Connection conn = getConnection(driver, url, user, password);
		PreparedStatement st = conn
				.prepareStatement("insert into student(sname,sno)values(?,?)");
		st.setString(1, "Zhangbing");
		st.setString(2, "001");
		int no = st.executeUpdate();

		st.close();
		conn.close();
	}

	private static void selectDemo() {
		try {
			// 加载驱动程序
			Connection conn = getConnection(driver, url, user, password);
			if (!conn.isClosed())
				System.out.println("Succeeded connecting to the Database!");

			// statement用来执行SQL语句
			Statement statement = conn.createStatement();
			String sql = "select * from student";
			ResultSet rs = statement.executeQuery(sql);
			ResultSetMetaData metaData = rs.getMetaData();
			System.out.println("-----------------");
			System.out.println("执行结果如下所示:");
			System.out.println("-----------------");
			System.out.println(" 学号" + "\t" + " 姓名");
			System.out.println("-----------------");
			String name = null;
			while (rs.next()) {
				name = rs.getString("sname");
				// name = new String(name.getBytes("ISO-8859-1"), "GB2312");
				System.out.println(rs.getString("sno") + "\t" + name);
			}
			rs.close();
			conn.close();
		} catch (ClassNotFoundException e) {
			System.out.println("Sorry,can`t find the Driver!");
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Connection getConnection(String driver, String url,
			String user, String password) throws ClassNotFoundException,
			SQLException {
		Class.forName(driver);

		// 连续数据库
		Connection conn = DriverManager.getConnection(url, user, password);
		return conn;
	}

}
