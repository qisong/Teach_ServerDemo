package com.gem.ptc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Entry
 */
@WebServlet("/Entry")
public class Entry extends HttpServlet {
	public static final int CURRENT_CLIENT_VERSION = 2;
	private static final long serialVersionUID = 1L;
	public static String IMAGE_FOLDER;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Entry() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) {
		response.setContentType("text/html");
		PrintWriter out;
		try {
			out = response.getWriter();
			response.setCharacterEncoding("UTF-8");
			out.println("Your input name:" + request.getParameter("name"));
			out.flush();
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
